var rail = {
	height: 960,
	width: 1160,
	SA2svg: null, 
	railsvg: null,
	stationsvg: null,
	overlaysvg: null,
	crime: {},
	maxCrime: 0,
	
	_spatialData: false,
	_crimeData: false,
	
	p: d3.geo.albers()
	    .parallels([-32, -31.8])
	    .rotate([-115,0])
	    .scale(320000)
	    .center([0,0])
	    .translate([-3400, -170700]),
	path: null,
	
	proj: function(x) {
		d3.selectAll("svg.background path")
			.attr("d", rail.path)
			.style("fill", function(d) {
				if ( d.geometry.type == 'Polygon' || d.geometry.type == 'MultiPolygon' ) {
					if ( !x ) {
						return 'black';
					} else {
						if ( rail.crime[d.properties.id] ) {
							var data = rail.crime[d.properties.id][x];
							if ( data ) {
								var strength = Math.min(data.crime / rail.maxCrime, 1) * 255;
								
								return '#'+Math.floor(strength).toString(16)+'0000';
							} else {
								return 'black';
							}
						}
					}
				}
			});
			
	},
	
	init: function() {
		rail.path = d3.geo.path().projection(rail.p);
	  
	  	rail.SA2svg = d3.select(".map").append("svg").classed({'background': true});
	  	rail.railsvg = d3.select(".map").append("svg").classed({'railline': true});
	  	rail.stationsvg = d3.select(".map").append("svg").classed({'station': true});
	  	rail.overlaysvg = d3.select(".map").append("svg").classed({'overlay': true});
	  	
	  	$('#radioButton2').prop('disabled',true);
	  	/*
	  	$('.station').on('click', function(evt) {
			$('.background').trigger('click', evt);
		});
		
		$('.background').on('click', function(evt) {
			rail.loadData($(this).data('sa2'));
		});
		*/
		
	},
	
	loadData: function(sa2id) {
		var rawData = rail.crime[sa2id];
		if ( rawData ) {
			currentSuburb = rawData.name;
		
			var outData = {
				color: '#ff0000',
				data: [],
				label: rawData.name
			};
		
			var timeRange = $('#footer').slider('option','max');
			
			for ( var x = 1; x <= timeRange; x++ ) {
				var data = rawData[x];
				
				if ( data ) {	
					outData.data.push([x, data.crime * data.population]);
				} else {
					outData.data.push([x, 0]);
				}
			}
		
			var eventData = {
				color: '#ddd',
				data: [],
				label: 'Game Time'
			};
			for ( var x = 1; x < 422; x++ ) {
				eventData.data.push([x, 0]);
			}
			for ( var x = 422; x < 428; x++ ) {
				eventData.data.push([x, 10]);
			}
			for ( var x = 428; x <= timeRange; x++ ) {
				eventData.data.push([x, 0]);
			}
			
			dataArray['radioButton1'] = [eventData, outData];
			plotAccordingToChoices();
		} else {
			$.plot('#graphArea', []);
		}
	},
	
	processCrimeData: function(res) {
		res = $.parseJSON(res);
		
		var baseYear = 2008;
		var maxTime = 0;
		var totalCrime = 0;
		
		$.each(res, function(idx, c) {
			var pop = Math.max(c.population, 2500);
			var cr = c.crimes / pop;
			
			totalCrime += cr;
			maxTime = Math.max(maxTime, (c.day * 24 + c.hour));
			rail.maxCrime = Math.max(rail.maxCrime, cr);
			
			if ( typeof rail.crime[c.sa2_id] === 'undefined' ) { 
				rail.crime[c.sa2_id] = {};
				rail.crime[c.sa2_id].name = c.suburb_name;
				
				rail.crime[c.sa2_id][(c.day * 24 + c.hour)] = { crime: cr, population: pop };
				
			} else {
				if ( typeof rail.crime[c.sa2_id][(c.day * 24 + c.hour)] === 'undefined' ) {
					rail.crime[c.sa2_id][(c.day * 24 + c.hour)] = { crime: cr, population: pop };
				} else {
					rail.crime[c.sa2_id][(c.day * 24 + c.hour)].crime += cr;
				}
			}
			
		});

		console.log(rail.crime);
		
		timer.init("#footer", 0, maxTime, 0, 50, rail.updateSlider);
	},
	
	updateSlider: function(evt) {
		var monthVal = $("#footer").slider('value');
		
		rail.proj(monthVal);
	}
};


$(function() {
	rail.init();
	
	d3.json("/geo/SA2_Perth.json", function(error, s) {
	  	rail._spatialData = s;
		  	 
	  	rail.SA2svg.append("g")
		  .selectAll("path")
			.data(topojson.feature(s, s.objects.layer1).features)
		  .enter().append("path")
		    .attr("d", rail.path)
			.attr("sa2", function(d) {
				return d.properties.id;
			})
			.attr("class", function(d) {
				if ( $.inArray(parseInt(d.properties.SA2_5DIG11,10), rail.active_sa2) >= 0 ) {
					return 'activeArea';
				} else {
					return '';
				}
			});
	    
	    rail.overlaysvg.append("g")
		  .selectAll("path")
			.data(topojson.feature(s, s.objects.layer1).features)
		  .enter().append("path")
		    .attr("d", rail.path)
			.attr("data-sa2", function(d) {
				return d.properties.id;
			});
	    
	    rail.proj();
	  	
	  	$('.overlay path').on('click', function() { 
	  		rail.loadData($(this).data('sa2'));
	  	});
	  	/*
	  	$('.overlay path').on('mouseout', function() { 
	  		$(this).removeClass('hover'); 
	  	});
	  	*/
		if ( rail._crimeData && rail._spatialData ) {
			rail.processCrimeData(rail._crimeData);
		}
	});
	
	d3.json("/php/landgate_get_railline.php", function(error, s) {
		rail.railsvg.append("g")
			.selectAll('path')
				.data(s.features)
			.enter().append("path")
		    	.attr("d", rail.path);
		    	
		rail.proj();
	});
	
	
	d3.json("/php/landgate_get_railallstation.php", function(error, s) {
		rail.stationsvg.append("g")
			.selectAll('path')
				.data(s.features)
			.enter().append("path")
		    	.attr("d", rail.path);
		    	
		rail.proj();
	});
	
	$.ajax('php/db_get_hour_crime.php?id=3', {success: function(res) {
		rail._crimeData = res;
		
		if ( rail._crimeData && rail._spatialData ) {
			rail.processCrimeData(rail._crimeData);
		}
	}});
});	  