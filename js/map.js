var x = 0;
var sa2id;
var dataIndexToDisplay;
var choiceContainer;

var currentSuburb = '';

idArray = {
	'radioButton1' : "By Month",
	'radioButton2' : "By Type",
};

var currentMapPlot;

var optOne = {
	xaxis: { 
		ticks: [ 
			 0,
			 [ 1, "2008" ], 
			 [ 13, "2010" ],
			 [ 25, "2011" ],
			 [ 37, "2012" ],
			 [ 49, "2013" ],
			 [ 61, "2014" ],
			 [ 73, "2015" ],
		],
		axisLabel: 'Year: Broken Down By Month',
		axisLabelUseCanvas: true,
		axisLabelFontSizePixels: 12,
		axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
		axisLabelPadding: 5,
	}
};
			
var optTwo = {
	xaxis: {
        minTickSize: 1,
        tickDecimals: 0
    },
    series: {
        bars: {
            show: true,
            barWidth: .9,
            align: "center"
        },
        stack: true
    },
    legend: {
    	show: false
    }
};



dataArray = {
	'radioButton1' : {
	},
	'radioButton2' : {
	},
};

$(function() {
	choiceContainer = $("#controlArea03");

	var plot;
	for ( i = 1; i <= 2; i++) {
		var radioButtonID = "radioButton" + i;
		var canvasID = "canvas_" + i;
		choiceContainer.append('<input type="radio" id="' + radioButtonID + '" name="radioButtons"/>');
		choiceContainer.append('<label for="' + radioButtonID + '">' + idArray[radioButtonID] + '<label/>');
		choiceContainer.append('<canvas id="' + canvasID + '" width="20" height="10"></canvas>');

		$('#' + radioButtonID + '').click(function() { plotAccordingToChoices;
		});
	}
	choiceContainer.buttonset();
	choiceContainer.find("input").click(plotAccordingToChoices);

	$('.timerControl').on('click', function() {
		switch ( $(this).data('action') ) {
			case 'play':
				timer.startTimer();
				break;
			case 'stop':
				timer.stopTimer();
				break;
		}
	});

	plotAccordingToChoices();
}); 



function plotAccordingToChoices() {
	var data = [];
	var key;

	var key = choiceContainer.find('input:checked').attr('id');
	if ( key ) {
		data = dataArray[key];
		
		var opt;
		if (key == "radioButton1") {
			opt = optOne;
		} else if (key == "radioButton2") {
			opt = optTwo;
		}
		
		if ( currentSuburb ) {
			$("#controlArea02").text("Current suburb: " + currentSuburb).show();
		} else {
			$("#controlArea02").hide();
		}
		currentMapPlot = $.plot("#graphArea", data, opt);
	}
};
