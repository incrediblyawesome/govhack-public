var rail = {
	height: 960,
	width: 1160,
	SA2svg: null, 
	railsvg: null,
	stationsvg: null,
	overlaysvg: null,
	crime: {},
	maxCrime: 0,
	
	_spatialData: false,
	_crimeData: false,
	
	p: d3.geo.albers()
	    .parallels([-34.1, -30.8])
	    .rotate([-115,0])
	    .scale(60000)
	    .center([0,0])
	    .translate([-400, -31700]),
	path: null,
	
	proj: function(x) {
		d3.selectAll("svg.background path")
			.attr("d", rail.path)
			.style("fill", function(d) {
				if ( d.geometry.type == 'Polygon' || d.geometry.type == 'MultiPolygon' ) {
					if ( !x ) {
						return 'black';
					} else {
						if ( rail.crime[d.properties.id] ) {
							var data = rail.crime[d.properties.id][x];
							if ( data ) {
								var strength = Math.min(data.crime / rail.maxCrime, 1) * 255;
								
								return '#'+Math.floor(strength).toString(16)+'0000';
							} else {
								return 'black';
							}
						}
					}
				}
			});
			
	},
	
	init: function() {
		rail.path = d3.geo.path().projection(rail.p);
	  
	  	rail.SA2svg = d3.select(".map").append("svg").classed({'background': true});
	  	rail.railsvg = d3.select(".map").append("svg").classed({'railline': true});
	  	rail.stationsvg = d3.select(".map").append("svg").classed({'station': true});
	  	rail.overlaysvg = d3.select(".map").append("svg").classed({'overlay': true});
	  	/*
	  	$('.station').on('click', function(evt) {
			$('.background').trigger('click', evt);
		});
		
		$('.background').on('click', function(evt) {
			rail.loadData($(this).data('sa2'));
		});
		*/
	},
	
	loadData: function(sa2_id) {
		var rawData = rail.crime[sa2_id];
		//Global var
		sa2id = sa2_id;
		
		$.ajax('/php/db_get_getcrimeforsa.php?sa2_id='+sa2id, {success: rail.processSACrimeData});
		
		if ( rawData ) {
			var outData = {
				color: '#ff0000',
				data: [],
				label: rawData.name
			};
		
			var timeRange = $('#footer').slider('option','max');
			
			for ( var x = 1; x <= timeRange; x++ ) {
				var data = rawData[x];
				
				if ( data ) {	
					outData.data.push([x, data.crime * data.population]);
				} else {
					outData.data.push([x, 0]);
				}
			}
			
			dataArray['radioButton1'] = [outData];
			plotAccordingToChoices();
		
		} else {
			$.plot('#graphArea', []);
		}
	},
	
	processSACrimeData: function(res) {
		res = $.parseJSON(res);
		colors = {
			2008: '#ff0000',
			2009: '#00ff00',
			2010: '#0000ff',
			2011: '#880000',
			2012: '#008800',
			2013: '#000088'
		};
		
		var categories = res[0];
		var rawData = res[1];
		
		var outData = [];
		
			
		$.each(categories, function(cIdx, cItem) {
			var catData = {
				label: cItem.NAME,
				data: [],
	            bars: {
	                show: true,
	            }
				
			};
			
			var yIdx = 0;
			for ( var y = 2008; y <= 2013; y++ ) {
				
				var yData = [];
				
				var cr = 0;
				
				$.each(rawData, function(idx, item) {
					if ( item.year == y && item.crime_category_id == cItem.ID ) {
						cr += parseInt(item.CRIMES,10);
					}
				});
				
				yIdx++;
				catData.data.push([y, cr]);
			}
			outData.push(catData);
		});
			
			
		dataArray['radioButton2'] = outData;
		plotAccordingToChoices();
		
	},
	
	processCrimeData: function(res) {
		res = $.parseJSON(res);
		
		var baseYear = 2008;
		var maxTime = 0;
		var totalCrime = 0;
		
		$.each(res, function(idx, c) {
			var pop = Math.max(c.POPULATION, 3500);
			var cr = c.CRIMES / pop;
			
			totalCrime += cr;
			maxTime = Math.max(maxTime, (c.YEAR - baseYear) * 12 + c.MONTH);
			rail.maxCrime = Math.max(rail.maxCrime, cr);
			
			if ( typeof rail.crime[c.SA2_ID] === 'undefined' ) { 
				rail.crime[c.SA2_ID] = {};
				rail.crime[c.SA2_ID].name = c.NAME;
				
				rail.crime[c.SA2_ID][(c.YEAR - baseYear) * 12 + c.MONTH] = { crime: cr, population: pop };
				
			} else {
				if ( typeof rail.crime[c.SA2_ID][(c.YEAR - baseYear) * 12 + c.MONTH] === 'undefined' ) {
					rail.crime[c.SA2_ID][(c.YEAR - baseYear) * 12 + c.MONTH] = { crime: cr, population: pop };
				} else {
					rail.crime[c.SA2_ID][(c.YEAR - baseYear) * 12 + c.MONTH].crime += cr;
				}
			}
			
		});

		console.log(rail.crime);
		
		timer.init("#footer", 0, maxTime, 0, 1, rail.updateSlider);
	},
	
	updateSlider: function(evt) {
		var monthVal = $("#footer").slider('value');
		
		rail.proj(monthVal);
	}
};


$(function() {
	rail.init();
	
	d3.json("/geo/SA2_Perth.json", function(error, s) {
	  	rail._spatialData = s;
		  	 
	  	rail.SA2svg.append("g")
		  .selectAll("path")
			.data(topojson.feature(s, s.objects.layer1).features)
		  .enter().append("path")
		    .attr("d", rail.path)
			.attr("sa2", function(d) {
				return d.properties.id;
			})
			.attr("class", function(d) {
				if ( $.inArray(parseInt(d.properties.SA2_5DIG11,10), rail.active_sa2) >= 0 ) {
					return 'activeArea';
				} else {
					return '';
				}
			});
	    
	    rail.overlaysvg.append("g")
		  .selectAll("path")
			.data(topojson.feature(s, s.objects.layer1).features)
		  .enter().append("path")
		    .attr("d", rail.path)
			.attr("data-sa2", function(d) {
				return d.properties.id;
			});
	    
	    rail.proj();
	  	
	  	$('.overlay path').on('click', function() { 
	  		rail.loadData($(this).data('sa2'));
	  	});
	  	/*
	  	$('.overlay path').on('mouseout', function() { 
	  		$(this).removeClass('hover'); 
	  	});
	  	*/
		if ( rail._crimeData && rail._spatialData ) {
			rail.processCrimeData(rail._crimeData);
		}
	});
	
	d3.json("/php/landgate_get_railline.php", function(error, s) {
		rail.railsvg.append("g")
			.selectAll('path')
				.data(s.features)
			.enter().append("path")
		    	.attr("d", rail.path);
		    	
		rail.proj();
	});
	
	
	d3.json("/php/landgate_get_railallstation.php", function(error, s) {
		rail.stationsvg.append("g")
			.selectAll('path')
				.data(s.features)
			.enter().append("path")
		    	.attr("d", rail.path);
		    	
		rail.proj();
	});
	
	$.ajax('php/db_get_suburbcrime.php?id=2', {success: function(res) {
		rail._crimeData = res;
		
		if ( rail._crimeData && rail._spatialData ) {
			rail.processCrimeData(rail._crimeData);
		}
	}});
});	  