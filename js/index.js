var rail = {
	height: 960,
	width: 1160,
	SA2svg: null, 
	railsvg: null,
	stationsvg: null,
	overlaysvg: null,
	crime: {},
	maxCrime: 0,
	
	_spatialData: false,
	_crimeData: false,
	
	p: d3.geo.albers()
	    .parallels([-34.1, -30.8])
	    .rotate([-115,0])
	    .scale(60000)
	    .center([0,0])
	    .translate([-400, -31700]),
	path: null,
	
	proj: function(x) {
		d3.selectAll("svg.background path")
			.attr("d", rail.path)
			.style("fill", function(d) {
				if ( d.geometry.type == 'Polygon' || d.geometry.type == 'MultiPolygon' ) {
					if ( !x ) {
						return 'black';
					} else {
						if ( rail.crime[d.properties.id] ) {
							var data = rail.crime[d.properties.id][x];
							if ( data ) {
								var strength = Math.min(data.crime / rail.maxCrime, 1) * 255;
								
								return '#'+Math.floor(strength).toString(16)+'0000';
							} else {
								return 'black';
							}
						}
					}
				}
			});
			
	},
	
	init: function() {
		rail.path = d3.geo.path().projection(rail.p);
	  
	  	rail.SA2svg = d3.select(".map").append("svg").classed({'background': true});
	  	rail.overlaysvg = d3.select(".map").append("svg").classed({'overlay': true});
	},
	
};


$(function() {
	rail.init();
	
	d3.json("/geo/SA2_Perth.json", function(error, s) {
	  	rail._spatialData = s;
		  	 
	  	rail.SA2svg.append("g")
		  .selectAll("path")
			.data(topojson.feature(s, s.objects.layer1).features)
		  .enter().append("path")
		    .attr("d", rail.path)
			.attr("sa2", function(d) {
				return d.properties.id;
			})
			.attr("class", function(d) {
				if ( $.inArray(parseInt(d.properties.SA2_5DIG11,10), rail.active_sa2) >= 0 ) {
					return 'activeArea';
				} else {
					return '';
				}
			});
	    
	    rail.proj();
	  	
		if ( rail._crimeData && rail._spatialData ) {
			rail.processCrimeData(rail._crimeData);
		}
	});
});	  