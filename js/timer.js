var timer = {
	_interval: 300,
	_timerHandle: null,
	_timer: null,
	_maxValue: 0,
	
	init: function(selector, minValue, maxValue, currentValue, interval, updateFunction) {
		timer._interval = interval;
		timer._maxValue = maxValue;
		
		timer._timer = $(selector).slider({
			min: minValue,
			max: maxValue,
			value: currentValue,
			change: updateFunction
		});
	},
	
	startTimer: function() {
		timer._timerHandle = window.setInterval(timer.incrementTimer, timer._interval);
	},
	
	stopTimer: function() {
		if ( timer._timerHandle ) {
			window.clearInterval(timer._timerHandle);
			timer._timerHandle = null;
		}
	},
	
	incrementTimer: function(stepValue) {
		if ( typeof stepValue === 'undefined' ) { stepValue = 1; }
		
		var currentVal = $("#footer").slider('value');
		if ( currentVal < timer._maxValue ) {
			timer._timer.slider('option','value', currentVal + stepValue);
		} else {
			timer.stopTimer();
		}
		
	}
};
