<!DOCTYPE HTML>
<html>
	<head>
		<?php
		include ('header.php');
		?>
		<title>Case Study: Mandurah Railway Line</title>
		
		<script type="text/javascript" src="js/map.js"></script>
		<script type="text/javascript" src="js/d3.min.js"></script>
		<script type="text/javascript" src="js/topojson.v1.min.js"></script>
		<script type="text/javascript" src="js/rail_crime.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.stack.min.js"></script>
		<script type="text/javascript" src="js/timer.js"></script>
	</head>
	<body>
		<div class="map"></div>
		<div class="header">
			<div id="headerMenu">
				<?php include ('navMenu.php');
				?>
			</div>
		</div>
		<div class="stuff">
			<div id="stuffDiv"></div>
			<div id="controlArea01">
			</div>
			<div id="controlArea02">
			</div>
			<div class="controlAreaScroll" id="controlArea03">
			</div>
			<div id="graphArea" style="width:100%;height:100%">
			</div>
		</div>
		<div class="controlArea">
		</div>
		<div class="footer">
			<div class="footControls">
				<a href="javascript:void(0);" class="timerControl" data-action="play">
					<img src="images/play.png" height=40 width=40/>
				</a>
				<a href="javascript:void(0);" class="timerControl" data-action="stop">
					<img src="images/pause.png" height=40 width=40/>
				</a>
			</div>
			<div class="footSlider" id="footer"></div>
		</div>
	</body>
</html>