<script>
	$(document).ready(function() {
		$('.myMenu > li').bind('mouseover', openSubMenu);
		$('.myMenu > li').bind('mouseout', closeSubMenu);
		
		function openSubMenu() {
			$(this).find('ul').css('visibility', 'visible');	
		};
		
		function closeSubMenu() {
			$(this).find('ul').css('visibility', 'hidden');	
		};
				   
	});
</script>

<div >
	<ul class="myMenu">
		<li>
			<a href="index.php">Project home</a>
		</li>
		<li>
			<a href="#">Case Studies</a>
			<ul>
				<li>
					<a href="perth.php">Perth/Metro Overview</a>
				</li>
				<li>
					<a href="map.php">Train Line: Mandurah Line</a>
				</li>
				<li>
					<a href="gameday.php">Game Day Analysis</a>
				</li>

			</ul>
		</li>
		<li>
			<a href="stats.php">Statistics Page</a>
		</li>
	</ul>
</div>
