Crime & Space

Crime happens, but...why? It's easy to make generalisations, but do they hold up against data?
Using time- and location- aware data from WA Police, combined with Landgate and ABS data, look at how demographics, public infrastructure and major events affect levels and types of crime in WA.

Team Members:
Bryce 'Glimmer'  Crage - ideas man, data wrangler
Paul 'Juffy'  Dowling - guy stuck between the ideas man and the UI dude
Rafer 'ArgyVonBargy'  Gluyas - UI dude

--------
Statistical findings: (in addition to shiny maps):

* Biggest reduction in crime over the last 5 years - Perth closely followed by Northbridge
* Biggest increase in crime over the last 5 years - Baldivis closely followed by Butler. Over the last 5 year crime has REDUCED!
* In the last 5 years Males have been victims more often than female
* In the last 5 years the age which is most vulnerable is 25 and 26 years olds - interestingly 20-29 year olds are the top 10.
* The most vulnerable category if Females who are 22 then a small jump back to Females who are 26 or 21.
* Biggest reduction in crime type in the last 5 years is Graffiti and Property Damage
* Biggest increase in crime type in the last 5 years is General Fraud then Domestic Assault
* Most crimes are reported in the morning between 9am and midday however domestic assault is more frequently reported between 9pm and midnight and Arson is most commonly reported at between midnight and 3am - Offensive disorderly is also reported more in the evening.
* Home Burglary most commonly occur better 7-9am and 10-12pm
* Some of the key crime types that have alcohol as a contributing factor when there is a large number of reported crimes include Disorderly (Offensive), Domestic Assault, Obstruct/Hinder Public Officer
* Theft is the highest crime type reported by nearly double any other crime.
* Whilst schools are on holiday average theft drops but there is an increase to property damage - however over all there is a general drop in reported crime
* There is less reported crime when its raining, however there are more drink driver, speed and liquor offences.

