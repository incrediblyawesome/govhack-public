<!DOCTYPE HTML>
<html>
	<head>
		<?php
		include ('header.php');
		?>
		<title>Acme Crime Lab Presents</title>
		
		<script type="text/javascript" src="js/map.js"></script>
		<script type="text/javascript" src="js/d3.min.js"></script>
		<script type="text/javascript" src="js/topojson.v1.min.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
		<script type="text/javascript" src="js/timer.js"></script>
	</head>
	<body>
		<div class="map"></div>

		<div class="header">
			<div id="headerMenu">
				<?php include ('navMenu.php');
				?>
			</div>
		</div>
		<div class="mainText">
			<div id="mainText">
				<H1>Crime & Space</H1>
				
				Crime happens, but...why? It's easy to make generalisations, but do they hold up against data?<p>

				Using time- and location- aware data from WA Police, combined with Landgate and ABS data, 
				look at how demographics, public infrastructure and major events affect levels and types of crime in WA.<p>
				
				Team Members:<br>
				Bryce <i>'Glimmer'&nbsp</i> Crage - ideas man, data wrangler<br>
				Paul <i>'Juffy'&nbsp</i> Dowling - guy stuck between the ideas man and the UI dude<br>
				Rafer <i>'ArgyVonBargy'&nbsp</i> Gluyas - UI dude<br>
				<br>
				<iframe width="560" height="315" src="//www.youtube.com/embed/H66FhLFm5yQ" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</body>
</html>