<!DOCTYPE HTML>
<html>
	<head>
		<?php
		include ('header.php');
		?>
		<title>Game day at Subiaco</title>
		
		<script type="text/javascript" src="js/map.js"></script>
		<script type="text/javascript" src="js/d3.min.js"></script>
		<script type="text/javascript" src="js/topojson.v1.min.js"></script>
		<script type="text/javascript" src="js/royalshow.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
		<script type="text/javascript" src="js/timer.js"></script>
	</head>
	<body>
		<div class="map"></div>
		<div class="header"></div>
		<div class="stuff">
			<div id="stuffDiv"></div>
			<div id="controlArea01">
			</div>
			<div id="controlArea02">
			</div>
			<div class="controlAreaScroll" id="controlArea03">
			</div>
			<div id="graphArea" style="width:600px;height:300px">
			</div>
		</div>
		<div class="controlArea">
		</div>
		<div class="footer">
			<?php
				include ('footer.php');
			?>
		</div>
	</body>
</html>