<?php
	$railline = isset($_GET["rail"]) ? $_GET["rail"] : '';
	
	include_once('apiKey.php');
	
	const RAILWAY_TABLE_ID = '09372590152434720789-15725712633632046983';

	if ( strlen($railline) > 0 ) {
		$filter = http_build_query(array(
			"select" => "featuretex,numberrail,railstatus,railgauge,railaccess,geometry",
			"where" => "featuretex = '".$railline."'"
		));
	} else {
		$filter = http_build_query(array(
			"select" => "featuretex,numberrail,railstatus,railgauge,railaccess,geometry",
			"where" => "featuretex = 'Midland Railway' OR featuretex = 'Armadale Railway' OR featuretex = 'Fremantle Railway' OR featuretex = 'Clarkson Railway' OR featuretex = 'Mandurah Railway'"
		));
	}
	
	$gme_url = "https://www.googleapis.com/mapsengine/v1/tables/" . RAILWAY_TABLE_ID . "/features?" . $filter . '&version=published&key='.APIKEY;
	
	$ch = curl_init($gme_url);

    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_HEADER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array("ApiKey: 21cbda586ef841c49444cc865aa9131b"));

  	if ( ($res = curl_exec( $ch )) === false ) {
  		die('cURL error: '.curl_error($ch));
  	}
	
	curl_close($ch); 
	
	print($res);
?>