<?php
ini_set('error_reporting',E_ALL);

	class db {
		private $mysqli = null;
		private $admin_email = 'jf@iinet.net.au';

		public function __construct() {
			//pdowling_govhack, $^Z,3xyr9oN3
			$this->mysqli = new mysqli("localhost", "root", "", "pdowling_govhack");
			if ($this->mysqli->connect_errno) {
		    	echo "Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
			}
		}
		
		public function getHourCrimeForScenario($scenarioid) {
			$result = $this->exec_prepared_stmt($this->mysqli,'SELECT *
																 FROM cache_suburb_hourly_crime_by_type
																WHERE scenario_id = ?','i', array($scenarioid));
			
			return $result;
		}
		
		
		public function getCrimeCategories() {
			$result = $this->exec_prepared_stmt($this->mysqli,'SELECT DISTINCT crime_category_id AS ID, crime_category AS NAME
																 FROM lut_crime_category','', array());
			
			return $result;
		}
		
		public function getCrimeDataPerSuburb($scenarioid) {
			$result = $this->exec_prepared_stmt($this->mysqli,'SELECT *
																 FROM cache_suburb_monthly_crime
																WHERE scenario_id = ?','i', array($scenarioid));
			
			return $result;
		}
		
		public function getCrimeDataForSa($sa2_id) {
			$result = $this->exec_prepared_stmt($this->mysqli,'SELECT sum(crimes) AS CRIMES, sum(crimes * severity) AS SEVERITY, month, year, sa2_id, suburb_name, crime_category_id, crime_category_name, population
																 FROM cache_suburb_monthly_crime_by_type c
																WHERE sa2_id = ?
																GROUP BY month, year, sa2_id, suburb_name, crime_category_id, crime_category_name, population
																ORDER BY year ASC, month ASC, sum(crimes * severity) DESC','i', array($sa2_id));
			
			
			return $result;
		}
		
		private function exec_prepared_stmt($dbConn, $sql, $types, $params) {
			$stmt = $dbConn->stmt_init();
			if(!$stmt->prepare($sql))
			{
			    //print "Failed to prepare statement: ".$stmt->error."<br/>";
			}
			
			if ( strlen($types) > 0 ) {
				array_unshift($params, $types);
				
				//Get a reference to the bind_param function, so we can call this thing with an arbitrary number of parameters.
				$ref    = new ReflectionClass('mysqli_stmt'); 
				$method = $ref->getMethod("bind_param"); 
				//Convert array to array of references to actual values before invoking.
				$method->invokeArgs($stmt,$this->makeValuesReferenced($params));
			}
			
			$stmt->execute() or die('<br/>MYSQL error: '.$stmt->error);
			$result = $this->fetch($stmt);
			$stmt->close();
			
			return $result;
		}
		
		private function fetch($result)
		{    
		    $array = array();
		    
		    if($result instanceof mysqli_stmt)
		    {
		        $result->store_result();
		        
		        $variables = array();
		        $data = array();
		        if ( $meta = $result->result_metadata() ) {
		        
			        while($field = $meta->fetch_field())
			            $variables[] = &$data[$field->name]; // pass by reference
			        
			        call_user_func_array(array($result, 'bind_result'), $variables);
			        
			        $i=0;
			        while($result->fetch())
			        {
			            $array[$i] = array();
			            foreach($data as $k=>$v)
			                $array[$i][$k] = $v;
			            $i++;
			        }
				}
		    }
		    elseif($result instanceof mysqli_result)
		    {
		        while($row = $result->fetch_assoc())
		            $array[] = $row;
		    }
		    
		    return $array;
		}
		
		//From: http://stackoverflow.com/questions/2045875/pass-by-reference-problem-with-php-5-3-1
		private function makeValuesReferenced($arr){
		    $refs = array();
		    foreach($arr as $key => $value)
		        $refs[$key] = &$arr[$key];
		    return $refs;
		
		}
	}