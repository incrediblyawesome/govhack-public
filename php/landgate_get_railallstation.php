<?php
	include_once('apiKey.php');
	
	const STOP_TABLE_ID = '09372590152434720789-08620406515972909896';

	$filter = http_build_query(array(
		"select" => "STOPTYPE,STOPNUMBER,LOCATION,geometry",
		"where" => "STOPTYPE = 'Rail' AND STOPNUMBER > 99000 AND STOPNUMBER < 99999"
	));
	
	$gme_url = "https://www.googleapis.com/mapsengine/v1/tables/" . STOP_TABLE_ID . "/features?" . $filter . '&version=published&key='.APIKEY;
	
	$ch = curl_init($gme_url);

    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_HEADER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array("ApiKey: 21cbda586ef841c49444cc865aa9131b"));

  	if ( ($res = curl_exec( $ch )) === false ) {
  		die('cURL error: '.curl_error($ch));
  	}
	
	curl_close($ch); 
	
	print($res);
?>