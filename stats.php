<!DOCTYPE HTML>
<html>
	<head>
		<?php
		include ('header.php');
		?>
		<title>Statistics : Fun facts and figures</title>
		
		<script type="text/javascript" src="js/map.js"></script>
		<script type="text/javascript" src="js/d3.min.js"></script>
		<script type="text/javascript" src="js/topojson.v1.min.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
		<script type="text/javascript" src="js/timer.js"></script>
	</head>
	<body>
		<div class="map"></div>

		<div class="header">
			<div id="headerMenu">
				<?php include ('navMenu.php');
				?>
			</div>
		</div>
		<div class="mainText">
			<div id="mainText">
				Biggest reduction in crime over the last 5 years - Perth closely followed by Northbridge<p>
				Biggest increase in crime over the last 5 years - Baldivis closely followed by Butler
				Over the last 5 year crime has REDUCED!<p>
				In the last 5 years Males have been victims more often than female<p>
				In the last 5 years the age which is most vulnerable is 25 and 26 years olds - interestingly 20-29 year olds are the top 10.<p>
				The most vulnerable category if Females who are 22 then a small jump back to Females who are 26 or 21.<p>
				Biggest reduction in crime type in the last 5 years is Graffiti and Property Damage<p>
				Biggest increase in crime type in the last 5 years is General Fraud then Domestic Assault<p>
				Most crimes are reported in the morning between 9am and midday however domestic assault is more frequently reported between 9pm and midnight and Arson is most commonly reported at between midnight and 3am - Offensive disorderly is also reported more in the evening.<p>
				Home Burglary most commonly occur better 7-9am and 10-12pm<p>
				Some of the key crime types that have alcohol as a contributing factor when there is a large number of reported crimes include Disorderly (Offensive), Domestic Assault, Obstruct/Hinder Public Officer<p>
				Theft is the highest crime type reported by nearly double any other crime.<p>
				Whilst schools are on holiday average theft drops but there is an increase to property damage - however over all there is a general drop in reported crime<p>
				There is less reported crime when its raining, however there are more drink driver, speed and liquor offences.<p>
			</div>
		</div>
	</body>
</html>